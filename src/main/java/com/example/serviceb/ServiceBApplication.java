package com.example.serviceb;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableFeignClients
@EnableCircuitBreaker
@RestController
public class ServiceBApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceBApplication.class);

    @Autowired
    private ServiceC serviceC;

    @GetMapping("/foo/c")
    public Map<String, String> cInfo(HttpServletRequest request) {
        LOGGER.info("Menerima request di {} : {}", request.getLocalAddr(), request.getLocalPort());
        return serviceC.serviceCInfo();
    }

    public static void main(String[] args) {
        SpringApplication.run(ServiceBApplication.class, args);
    }
}

@FeignClient(name = "serviceC", fallback = ServiceCFallback.class)
interface ServiceC {

    @GetMapping("/foo/info")
    public Map<String, String> serviceCInfo();
}

@Component
class ServiceCFallback implements ServiceC {

    @Override
    public Map<String, String> serviceCInfo() {
        Map<String, String> hasil = new HashMap<>();
        hasil.put("status", "c not available");
        return hasil;
    }

}
